package com.pdf.generatepdf.controller;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.borders.DashedBorder;
import com.itextpdf.layout.element.*;
import com.itextpdf.layout.properties.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.google.zxing.WriterException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import com.itextpdf.layout.element.List;
import com.itextpdf.layout.element.ListItem;


@RestController
@RequestMapping("/api/pdf")
public class GeneratePdf {

    private static PdfFont generalFontRegular;

    public static void main(String[] args) {
        try {
            // Load a font file for regular style (change the file path as needed)
            generalFontRegular = PdfFontFactory.createFont("/home/kazi/Downloads/generate-pdf/AscenderSansRegular.ttf", String.valueOf(true));

            // Rest of your code...
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @GetMapping("/generate")
    public ResponseEntity<byte[]> generateAndDownloadPdf() throws IOException, WriterException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PdfWriter pdfWriter = new PdfWriter(baos);
        PdfDocument pdfDocument = new PdfDocument(pdfWriter);
        pdfDocument.setDefaultPageSize(PageSize.A4);
        Document document = new Document(pdfDocument);


//        // Create a QR code and convert it to an image
//        String content = "https://www.example.com";
//        Writer writer = new QRCodeWriter();
//        BitMatrix bitMatrix = writer.encode(content, BarcodeFormat.QR_CODE, 200, 200);
//        Image qrCodeImage = new Image(ImageDataFactory.create(createBitMatrixImage(bitMatrix, new DeviceRgb(0, 0, 0))));



        Table tableHeader = new Table(3);
        tableHeader.setWidth(UnitValue.createPercentValue(100));
        tableHeader.addCell(createCell("Student\n" +
                "Photo").setTextAlignment(TextAlignment.CENTER).setVerticalAlignment(VerticalAlignment.MIDDLE));

        PdfFont headerFont = PdfFontFactory.createFont("/home/kazi/Downloads/generate-pdf/assets/font/AgencyFbBold.ttf");
        PdfFont addressFont = PdfFontFactory.createFont("/home/kazi/Downloads/generate-pdf/assets/font/AscenderSerif.ttf");
        PdfFont informationFont = PdfFontFactory.createFont("/home/kazi/Downloads/generate-pdf/assets/font/FoundryFormBold.ttf");
        PdfFont informationFontItalic = PdfFontFactory.createFont("/home/kazi/Downloads/generate-pdf/assets/font/LiberationSansBoldItalic.ttf");
        PdfFont generalFontRegular = PdfFontFactory.createFont("/home/kazi/Downloads/generate-pdf/assets/font/AscenderSansRegular.ttf");
        PdfFont generalFontBold = PdfFontFactory.createFont("/home/kazi/Downloads/generate-pdf/assets/font/AscenderSansBold.ttf");

        Cell universityCell = createCell("");
        universityCell.setBorder(Border.NO_BORDER);
        universityCell.add(new Paragraph("SOUTHEAST UNIVERSITY").setFont(headerFont)
                .setFontSize(20)
                .setTextAlignment(TextAlignment.CENTER)
                .setMarginTop(-15));
        universityCell.add(new Paragraph("252, Tejgaon I/A, Dhaka 1208, Bangladesh").setFont(addressFont)
                .setFontSize(8)
                .setTextAlignment(TextAlignment.CENTER)
                .setMarginBottom(7));
        universityCell.add(new Paragraph("Admit Card").setFont(informationFont)
                .setFontSize(14)
                .setTextAlignment(TextAlignment.CENTER));
        universityCell.add(new Paragraph("Final Examination")
                .setFont(informationFontItalic)
                .setFontSize(12)
                .setTextAlignment(TextAlignment.CENTER));
        universityCell.add(new Paragraph("Fall 2023")
                .setFont(informationFontItalic)
                .setFontSize(12)
                .setTextAlignment(TextAlignment.CENTER));
        tableHeader.addCell(universityCell);

        tableHeader.addCell(createCell("Student\n" +
                "Photo").setTextAlignment(TextAlignment.CENTER).setVerticalAlignment(VerticalAlignment.MIDDLE));



//        // Adding rows to the table
//        for (int i = 1; i <= 5; i++) {
//            table.addCell(createCell("Row " + i));
//            table.addCell(createCell("Data " + i));
//            table.addCell(createCell("Value " + i));
//        }

        document.add(tableHeader);

        Table tableStudentInformation = new Table(14)
                .setMarginTop(10)
                .setWidth(UnitValue.createPercentValue(70))
                .setHorizontalAlignment(HorizontalAlignment.CENTER);

        tableStudentInformation.addCell(createCell("Student Code").setFont(generalFontBold)
                .setFontSize(10)
                .setTextAlignment(TextAlignment.CENTER)
                .setPaddings(0,-15,0,-15));

        String studentCode = "2020000000055";
        for (char digitChar : studentCode.toCharArray()) {
            tableStudentInformation.addCell(createCell(String.valueOf(digitChar)).setFont(generalFontBold)
                    .setFontSize(10)
                    .setTextAlignment(TextAlignment.CENTER)
                    .setPaddings(0, 0,0,0));
        }

        tableStudentInformation.addCell(createCell("Name").setFont(generalFontRegular)
                .setFontSize(10).setPadding(-2)
                .setTextAlignment(TextAlignment.CENTER)
                .setVerticalAlignment(VerticalAlignment.MIDDLE)
                .setPaddings(0, 0,0,0));

        tableStudentInformation.addCell(new Cell(0, 13).add(new Paragraph("Md. Shakhawat Hossain"))
                .setFont(generalFontRegular)
                .setFontSize(10)
                .setPaddingLeft(4));

        tableStudentInformation.addCell(createCell("Program").setFont(generalFontRegular)
                .setFontSize(10)
                .setTextAlignment(TextAlignment.CENTER)
                .setVerticalAlignment(VerticalAlignment.MIDDLE)
                .setPaddings(0, 0,0,0));

        tableStudentInformation.addCell(new Cell(0, 13).add(new Paragraph("BSc in CSE, Batch # 54"))
                .setFont(generalFontRegular)
                .setFontSize(10)
                .setPaddingLeft(4));

        document.add(tableStudentInformation);

        document.add(new Paragraph("Registered Courses:")
                .setFont(generalFontBold)
                .setFontSize(10)
                .setMarginTop(12)
                .setTextAlignment(TextAlignment.LEFT));


        // Adding course list table
        Table tableCourseInformation = new Table(6)
                .setMarginTop(-5)
                .setWidth(UnitValue.createPercentValue(100))
                .setHorizontalAlignment(HorizontalAlignment.CENTER);

        tableCourseInformation.addCell(createCell("Course Code").setFont(generalFontBold)
                .setFontSize(10)
                .setTextAlignment(TextAlignment.LEFT)
                .setVerticalAlignment(VerticalAlignment.MIDDLE)
                .setPaddingLeft(4));

        tableCourseInformation.addCell(createCell("Course Title                         ", 3, 1).setFont(generalFontBold)
                .setFontSize(10)
                .setTextAlignment(TextAlignment.LEFT)
                .setVerticalAlignment(VerticalAlignment.MIDDLE)
                .setPaddingLeft(4));


        tableCourseInformation.addCell(createCell("Registration\n" +
                "Type").setFont(generalFontBold)
                .setFontSize(10)
                .setTextAlignment(TextAlignment.CENTER)
                .setVerticalAlignment(VerticalAlignment.MIDDLE));

        tableCourseInformation.addCell(createCell("Invigilator's\n" +
                "Signature").setFont(generalFontBold)
                .setFontSize(10)
                .setTextAlignment(TextAlignment.CENTER)
                .setVerticalAlignment(VerticalAlignment.MIDDLE));


        ///Course List
        tableCourseInformation.addCell(createCell("CSE489.1").setFont(generalFontRegular)
                .setFontSize(10)
                .setTextAlignment(TextAlignment.LEFT)
                .setPaddingLeft(4));

        tableCourseInformation.addCell(createCell("Internship", 3, 1).setFont(generalFontRegular)
                .setFontSize(10)
                .setTextAlignment(TextAlignment.LEFT)
                .setPaddingLeft(4));


        tableCourseInformation.addCell(createCell("Regular"+"\n"+" \n").setFont(generalFontRegular)
                .setFontSize(10)
                .setTextAlignment(TextAlignment.CENTER));

        tableCourseInformation.addCell(createCell(""));


        tableCourseInformation.addCell(createCell("CSE459.1").setFont(generalFontRegular)
                .setFontSize(10)
                .setTextAlignment(TextAlignment.LEFT)
                .setPaddingLeft(4));

        tableCourseInformation.addCell(createCell("Research ", 3, 1).setFont(generalFontRegular)
                .setFontSize(10)
                .setTextAlignment(TextAlignment.LEFT)
                .setPaddingLeft(4));


        tableCourseInformation.addCell(createCell("Regular"+"\n"+" \n").setFont(generalFontRegular)
                .setFontSize(10)
                .setTextAlignment(TextAlignment.CENTER));

        tableCourseInformation.addCell(createCell(""));


        LocalDateTime currentDateTime = LocalDateTime.now();
        tableCourseInformation.addCell(createCell("Generated Time: " + currentDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"))+"\n"+" \n", 2,1)
                .setBorderRight(Border.NO_BORDER)
                .setBorderLeft(Border.NO_BORDER)
                .setBorderBottom(new DashedBorder(.7f))
                .setFont(generalFontRegular)
                .setFontSize(8)
                .setTextAlignment(TextAlignment.LEFT));

        tableCourseInformation.addCell(createCell("This file was generated from UMS without any alteration or erasure."+"\n"+" \n", 4, 1).setFont(generalFontRegular)
                .setBorderRight(Border.NO_BORDER)
                .setBorderLeft(Border.NO_BORDER)
                .setBorderBottom(new DashedBorder(.7f))
                .setFontSize(8)
                .setTextAlignment(TextAlignment.RIGHT));

        document.add(tableCourseInformation);


        document.add(new Paragraph("Instructions to the Examine:")
                .setFont(generalFontBold)
                .setFontSize(10)
                .setMarginTop(20)
                .setTextAlignment(TextAlignment.LEFT));




        String[] dataArray = {
                "Print this Admit Card on an A4 size paper. Colour print is not mandatory.",
                "Admit card is valid only if the examines photograph is legibly printed.",
                "The examine must bring the ‘Admit Card’ and ‘Student ID’ to the examination hall and show those to the invigilators when they ask.",
                "The examine must arrive in the examination hall at least 15 minutes before the start of the examination.",
                "The examine must comply with the directions given by the invigilators at the examination hall.",
                "Use of cell phones, programmable calculators, smartwatches, and other electronic devices is prohibited in the examination hall.",
                "The examine involved in unfair means/misconduct in the examination hall will be expelled from the examination."
        };

        // Adding course list table
//        Table tableExamineInstruction = new Table(1)
//                .setMarginTop(-5)
//                .setWidth(UnitValue.createPercentValue(100))
//                .setHorizontalAlignment(HorizontalAlignment.CENTER);


        document.add(createCellWithArray(dataArray)
                .setPaddingLeft(20)
                .setMarginTop(-5));

//        document.add(createUnorderedList(dataArray));

//        document.add(tableExamineInstruction);




        document.close();


        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_PDF);
        headers.setContentDispositionFormData("attachment", "generated-pdf.pdf");

        return ResponseEntity.ok()
                .headers(headers)
                .body(baos.toByteArray());
    }

//    private static List createUnorderedList(String[] dataArray) {
//        List unorderedList = new List();
//        for (String line : dataArray) {
//            unorderedList.add(new ListItem(line)
//                    .setFont(generalFontRegular)
//                    .setFontSize(10)
//                    .setTextAlignment(TextAlignment.LEFT)
//                    .setPaddingLeft(20)
//                    .setListSymbol("- ")
//                    .setListSymbolAlignment(ListSymbolAlignment.LEFT));
//        }
//        return unorderedList;
//    }

    private static List createCellWithArray(String[] dataArray) throws IOException {
        PdfFont generalFontRegular = PdfFontFactory.createFont("/home/kazi/Downloads/generate-pdf/asset/font/AscenderSansRegular.ttf");
        List unorderedList = new List();
        for (String line : dataArray) {
            unorderedList.add((ListItem) new ListItem(line)
                    .setFont(generalFontRegular)
                    .setFontSize(10)
                    .setTextAlignment(TextAlignment.LEFT));
        }
        return unorderedList;
    }

    private static Cell createCell(String content) {
        return createCell(content, null, null);
    }

    private static Cell createCell(String content, Integer colSpan) {
        return createCell(content, colSpan, null);
    }

    private static Cell createCell(String content, Integer colSpan, Integer rowSpan) {

        if (colSpan != null && rowSpan != null) {
            Cell cell = new Cell(rowSpan, colSpan).setTextAlignment(TextAlignment.CENTER);
            cell.add(new Paragraph(content));
            return cell;
        }else if (colSpan != null && rowSpan == null) {
            Cell cell = new Cell(0, colSpan).setTextAlignment(TextAlignment.CENTER);
            cell.add(new Paragraph(content));
            return cell;
        }else if (colSpan == null && rowSpan != null) {
            Cell cell = new Cell(rowSpan, 0).setTextAlignment(TextAlignment.CENTER);
            cell.add(new Paragraph(content));
            return cell;
        }

        Cell cell = new Cell().setTextAlignment(TextAlignment.CENTER);
        cell.add(new Paragraph(content));
        return cell;

    }

//    private static BufferedImage createBitMatrixImage(BitMatrix bitMatrix, Color color) {
//        int width = bitMatrix.getWidth();
//        int height = bitMatrix.getHeight();
//        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
//
//        for (int x = 0; x < width; x++) {
//            for (int y = 0; y < height; y++) {
//                image.setRGB(x, y, bitMatrix.get(x, y) ? color.getRGB() : 0xFFFFFF);
//            }
//        }
//
//        return image;
//    }


}