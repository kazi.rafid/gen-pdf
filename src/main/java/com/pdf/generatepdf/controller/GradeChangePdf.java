package com.pdf.generatepdf.controller;
import com.google.zxing.WriterException;
import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.kernel.events.Event;
import com.itextpdf.kernel.events.IEventHandler;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.borders.DashedBorder;
import com.itextpdf.layout.borders.SolidBorder;
import com.itextpdf.layout.element.*;
import com.itextpdf.layout.properties.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


@RestController
@RequestMapping("/api/gradeChange")
class GradeChangePDF {
    LocalDateTime currentDateTime = LocalDateTime.now();

    @GetMapping("/pdf")
    public ResponseEntity<byte[]> generateAndDownloadPdf() throws IOException, WriterException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PdfWriter pdfWriter = new PdfWriter(baos);
        PdfDocument pdfDocument = new PdfDocument(pdfWriter);
        Document document = new Document(pdfDocument);

        // Create PageEventHandler and set the Document reference
        PageEventHandler pageEventHandler = new PageEventHandler();
        pageEventHandler.setDocument(document);

        // Attach the PageEventHandler to the PdfDocument
        pdfDocument.addEventHandler(PdfDocumentEvent.END_PAGE, pageEventHandler);

        pdfDocument.setDefaultPageSize(PageSize.A4);
        document.setMargins(10,20,30,20);

        Table tableHeader = new Table(3);
        tableHeader.setWidth(UnitValue.createPercentValue(100));
        tableHeader.addCell(createCell("Student\n" +
                "Photo").setTextAlignment(TextAlignment.CENTER).setVerticalAlignment(VerticalAlignment.MIDDLE));


        PdfFont agencyFbFont = PdfFontFactory.createFont("/home/kazi/Downloads/generate-pdf/assets/font/AgencyFb.ttf");
        PdfFont helveticaBold = PdfFontFactory.createFont(StandardFonts.HELVETICA_BOLD);
        PdfFont helvetica = PdfFontFactory.createFont(StandardFonts.HELVETICA);


        Paragraph p1 = new Paragraph()
                .add("SOUTHEAST UNIVERSITY")
                .setTextAlignment(TextAlignment.CENTER)
                .setFont(agencyFbFont)
                .setFontSize(20)
                .setBold()
                .setMargin(0)
                .setPadding(0);
        document.add(p1);

        Paragraph p2 = new Paragraph()
                .add("252, Tejgaon I/A, Dhaka 1208, Bangladesh" + "\n" + "Telephone: 8802226603610-7" + "; " + "Email: info@seu.edu.bd")
                .setFontSize(10)
                .setFont(helvetica)
                .setTextAlignment(TextAlignment.CENTER)
                .setMargin(0)
                .setPadding(0);
        document.add(p2);

        Paragraph p3 = new Paragraph()
                .add("Grade Change")
                .setTextAlignment(TextAlignment.CENTER)
                .setFont(helveticaBold)
                .setFontSize(15)
                .setMarginTop(10)
                .setMarginBottom(-7)
                .setPadding(0);
        document.add(p3);


        Paragraph p33 = new Paragraph()
                .add("Application ID # " + "866010")
                .setTextAlignment(TextAlignment.CENTER)
                .setFont(helveticaBold)
                .setFontSize(12)
                .setMarginTop(10)
                .setMarginBottom(-7)
                .setPadding(0);
        document.add(p33);



        int fontSize = 10;
        float fontSize8 = 10;

        {
            float[] signColumnWidth = {15, 32, 2, 15, 32};
            Table tableSign = new Table(UnitValue.createPercentArray(signColumnWidth));
            tableSign.setBorder(Border.NO_BORDER);
            tableSign.setMarginTop(10);
            tableSign.setWidth(UnitValue.createPercentValue(100));

            Paragraph w1 = new Paragraph().add("Student Information:")
                    .setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
            Cell cell1 = new Cell(1, 5).add(w1).setBorder(Border.NO_BORDER);
            tableSign.addCell(cell1);
            {
                Paragraph w11 = new Paragraph().add("Student Name").setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
                Cell cell11 = new Cell().add(w11).setBorder(new SolidBorder(1));
                tableSign.addCell(cell11);

                Paragraph w12 = new Paragraph().add("Kazi Shahrier Rafid").setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
                Cell cell12 = new Cell().add(w12).setBorder(new SolidBorder(1));
                tableSign.addCell(cell12);

                Paragraph w13 = new Paragraph().add(" ").setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
                Cell cell13 = new Cell().add(w13).setBorder(Border.NO_BORDER);
                tableSign.addCell(cell13);


                Paragraph w14 = new Paragraph().add("Student ID").setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
                Cell cell14 = new Cell().add(w14).setBorder(new SolidBorder(1));
                tableSign.addCell(cell14);

                Paragraph w15 = new Paragraph().add("0232320002161026").setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
                Cell cell15 = new Cell().add(w15).setBorder(new SolidBorder(1));
                tableSign.addCell(cell15);
            }

            {
                String fatherName = "Kazi Mohammed Shakhawat Hossain";
//                if (student.getFather() != null && student.getFather().getName() != null) {
//                    fatherName = student.getFather().getName().getFullName().trim();
//                }
                Paragraph w11 = new Paragraph().add("Father Name").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize);
                Cell cell11 = new Cell().add(w11).setBorder(new SolidBorder(1));
                tableSign.addCell(cell11);

                Paragraph w12 = new Paragraph().add(fatherName).setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize);
                Cell cell12 = new Cell().add(w12).setBorder(new SolidBorder(1));
                tableSign.addCell(cell12);

                Paragraph w13 = new Paragraph().add(" ").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize);
                Cell cell13 = new Cell().add(w13).setBorder(Border.NO_BORDER);
                tableSign.addCell(cell13);

                Paragraph w14 = new Paragraph().add("Student Code").setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
                Cell cell14 = new Cell().add(w14).setBorder(new SolidBorder(1));
                tableSign.addCell(cell14);

                Paragraph w15 = new Paragraph().add("2019000000034").setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
                Cell cell15 = new Cell().add(w15).setBorder(new SolidBorder(1));
                tableSign.addCell(cell15);
            }

            {
                String emailAddress = "2019000000034";
//                if (student.getEmailAddressList() != null) {
//                    emailAddress = student.getEmailAddressList().get(0).getAddress();
//                }
//                if (emailAddress.isEmpty()) {
//                    emailAddress = student.getId() + "@seu.edu.bd";
//                }
                Paragraph w11 = new Paragraph().add("Email").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize);
                Cell cell11 = new Cell().add(w11).setBorder(new SolidBorder(1));
                tableSign.addCell(cell11);

                Paragraph w12 = new Paragraph().add(emailAddress).setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize);
                Cell cell12 = new Cell().add(w12).setBorder(new SolidBorder(1));
                tableSign.addCell(cell12);

                Paragraph w13 = new Paragraph().add(" ").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize);
                Cell cell13 = new Cell().add(w13).setBorder(Border.NO_BORDER);
                tableSign.addCell(cell13);

                Paragraph w14 = new Paragraph().add("Program Name").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize);
                Cell cell14 = new Cell().add(w14).setBorder(new SolidBorder(1));
                tableSign.addCell(cell14);

                Paragraph w15 = new Paragraph().add("BSc in CSE" + ", Batch: " + "51").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize);
                Cell cell15 = new Cell().add(w15).setBorder(new SolidBorder(1));
                tableSign.addCell(cell15);
            }

            {
                Paragraph w11 = new Paragraph().add("Mobile No.").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize);
                Cell cell11 = new Cell().add(w11).setBorder(new SolidBorder(1));
                tableSign.addCell(cell11);

                Paragraph w12 = new Paragraph().add("01751610399").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize);
                Cell cell12 = new Cell().add(w12).setBorder(new SolidBorder(1));
                tableSign.addCell(cell12);

                Paragraph w13 = new Paragraph().add(" ").setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
                Cell cell13 = new Cell().add(w13).setBorder(Border.NO_BORDER);
                tableSign.addCell(cell13);

                Paragraph w14 = new Paragraph().add("Admitted Semester").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize - 2);
                Cell cell14 = new Cell().add(w14).setBorder(new SolidBorder(1));
                tableSign.addCell(cell14);

                Paragraph w15 = new Paragraph().add("Spring 2019").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize);
                Cell cell15 = new Cell().add(w15).setBorder(new SolidBorder(1));
                tableSign.addCell(cell15);
            }

            document.add(tableSign);
        }
        // ----------------------------//
        // ----------------//
        {
            float[] signColumnWidth = {15, 40, 2, 15, 25};
            Table tableSign = new Table(UnitValue.createPercentArray(signColumnWidth));
            tableSign.setBorder(Border.NO_BORDER);
            tableSign.setMarginTop(10);
            tableSign.setWidth(UnitValue.createPercentValue(100));

            Paragraph w1 = new Paragraph().add("Application Information:")
                    .setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
            Cell cell1 = new Cell(1, 8).add(w1).setBorder(Border.NO_BORDER);
            tableSign.addCell(cell1);
            {

                Paragraph w16 = new Paragraph().add("Faculty:").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8);
                Cell cell16 = new Cell().add(w16).setBorder(new SolidBorder(1));
                tableSign.addCell(cell16);

                Paragraph w17 = new Paragraph().add("Asaduzzaman Noor").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8);
                Cell cell17 = new Cell().add(w17).setBorder(new SolidBorder(1));
                tableSign.addCell(cell17);

                Paragraph w100 = new Paragraph().add(" ").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8);
                Cell w1001 = new Cell().add(w100).setBorder(Border.NO_BORDER);
                tableSign.addCell(w1001);

                Paragraph w19 = new Paragraph().add("Applied Date:").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8);
                Cell cell19 = new Cell().add(w19).setBorder(new SolidBorder(1));
                tableSign.addCell(cell19);

                Paragraph w20 = new Paragraph().add(currentDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))).setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8);
                Cell cell20 = new Cell().add(w20).setBorder(new SolidBorder(1));
                tableSign.addCell(cell20);

                Paragraph w190 = new Paragraph().add("Designation:").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8);
                Cell cell190 = new Cell().add(w190).setBorder(new SolidBorder(1));
                tableSign.addCell(cell190);

                Paragraph w2000 = new Paragraph().add("Assistant Professor, Department of Computer Science & Engineering").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8);
                Cell cell2000 = new Cell().add(w2000).setBorder(new SolidBorder(1));
                tableSign.addCell(cell2000);

                Paragraph w1000 = new Paragraph().add(" ").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8);
                Cell w10001 = new Cell().add(w1000).setBorder(Border.NO_BORDER);
                tableSign.addCell(w10001);

//                Paragraph w1900 = new Paragraph().add("Application Priority:").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8);
//                Cell cell1900 = new Cell().add(w1900).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell1900);
//
//                Paragraph w200 = new Paragraph().add("URGENT").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8);
//                Cell cell200 = new Cell().add(w200).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell200);
            }

            document.add(tableSign);
        }

        {
            float[] signColumnWidth = {15, 40, 2, 15, 25};
            Table tableSign = new Table(UnitValue.createPercentArray(signColumnWidth));
            tableSign.setBorder(Border.NO_BORDER);
            tableSign.setMarginTop(10);
            tableSign.setWidth(UnitValue.createPercentValue(100));

            Paragraph w1 = new Paragraph().add("Course Information:")
                    .setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
            Cell cell1 = new Cell(1, 5).add(w1).setBorder(Border.NO_BORDER);
            tableSign.addCell(cell1);
            {
                Paragraph w11 = new Paragraph().add("Course Code:").setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
                Cell cell11 = new Cell().add(w11).setBorder(new SolidBorder(1));
                tableSign.addCell(cell11);

                Paragraph w12 = new Paragraph().add("CSE281").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize);
                Cell cell12 = new Cell().add(w12).setBorder(new SolidBorder(1));
                tableSign.addCell(cell12);

                Paragraph w13 = new Paragraph().add(" ").setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
                Cell cell13 = new Cell().add(w13).setBorder(Border.NO_BORDER);
                tableSign.addCell(cell13);


                Paragraph w14 = new Paragraph().add("Section:").setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
                Cell cell14 = new Cell().add(w14).setBorder(new SolidBorder(1));
                tableSign.addCell(cell14);

                Paragraph w15 = new Paragraph().add("10").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize);
                Cell cell15 = new Cell().add(w15).setBorder(new SolidBorder(1));
                tableSign.addCell(cell15);

                Paragraph w16 = new Paragraph().add("Course Name:").setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
                Cell cell16 = new Cell().add(w16).setBorder(new SolidBorder(1));
                tableSign.addCell(cell16);

                Paragraph w17 = new Paragraph().add("Introduction to Programming Language II (Java)").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize);
                Cell cell17 = new Cell().add(w17).setBorder(new SolidBorder(1));
                tableSign.addCell(cell17);

                Paragraph w18 = new Paragraph().add(" ").setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
                Cell cell18 = new Cell().add(w18).setBorder(Border.NO_BORDER);
                tableSign.addCell(cell18);

                Paragraph w19 = new Paragraph().add("Semester:").setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
                Cell cell19 = new Cell().add(w19).setBorder(new SolidBorder(1));
                tableSign.addCell(cell19);

                Paragraph w20 = new Paragraph().add("Spring 2024").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize);
                Cell cell20 = new Cell().add(w20).setBorder(new SolidBorder(1));
                tableSign.addCell(cell20);
            }

            document.add(tableSign);
        }

        {
            float[] signColumnWidth = {15, 32, 2, 15, 32};
            Table tableSign = new Table(UnitValue.createPercentArray(signColumnWidth));
            tableSign.setBorder(Border.NO_BORDER);
            tableSign.setMarginTop(10);
            tableSign.setWidth(UnitValue.createPercentValue(100));

            Paragraph w1 = new Paragraph().add("Grade Information:")
                    .setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
            Cell cell1 = new Cell(1, 5).add(w1).setBorder(Border.NO_BORDER);
            tableSign.addCell(cell1);
            {
                Paragraph w16 = new Paragraph().add("Grade assigned").setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
                Cell cell16 = new Cell().add(w16).setBorder(new SolidBorder(1));
                tableSign.addCell(cell16);

                Paragraph w17 = new Paragraph().add("I").setTextAlignment(TextAlignment.CENTER).setFont(helveticaBold).setFontSize(20);
                Paragraph w177 = new Paragraph().add("(incomplete)").setTextAlignment(TextAlignment.CENTER).setFont(helvetica).setFontSize(8);
                Cell cell17 = new Cell().add(w17).setBorder(new SolidBorder(1));
                cell17.add(w177);
                tableSign.addCell(cell17);

                Paragraph w18 = new Paragraph().add(" ").setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
                Cell cell18 = new Cell().add(w18).setBorder(Border.NO_BORDER);
                tableSign.addCell(cell18);

                Paragraph w19 = new Paragraph().add("Change to").setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
                Cell cell19 = new Cell().add(w19).setBorder(new SolidBorder(1));
                tableSign.addCell(cell19);

                Paragraph w20 = new Paragraph().add("B-").setTextAlignment(TextAlignment.CENTER).setFont(helveticaBold).setFontSize(20);
                Paragraph w200 = new Paragraph().add("(minus)").setTextAlignment(TextAlignment.CENTER).setFont(helvetica).setFontSize(8);
                Cell cell20 = new Cell().add(w20).setBorder(new SolidBorder(1));
                cell20.add(w200);
                tableSign.addCell(cell20);
            }

            document.add(tableSign);
        }

        {
            float[] signColumnWidth = {100};
            Table tableSign = new Table(UnitValue.createPercentArray(signColumnWidth));
            tableSign.setBorder(Border.NO_BORDER);
            tableSign.setMarginTop(10);
            tableSign.setWidth(UnitValue.createPercentValue(100));

            Paragraph w1 = new Paragraph().add("Reason for Grade Change:")
                    .setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
            Cell cell1 = new Cell().add(w1).setBorder(Border.NO_BORDER);
            tableSign.addCell(cell1);
            {
                Paragraph w20 = new Paragraph().add("A quick brown fox jumps over the lazy dog.").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8);
                Cell cell20 = new Cell().add(w20).setBorder(new SolidBorder(1));
                tableSign.addCell(cell20);
            }
            document.add(tableSign);
        }

        {
            float[] signColumnWidth = {25, 12, 25, 12, 25};
            Table tableSign = new Table(UnitValue.createPercentArray(signColumnWidth));
            tableSign.setBorder(Border.NO_BORDER);
            tableSign.setMarginTop(60);
            tableSign.setMarginBottom(40);
            tableSign.setWidth(UnitValue.createPercentValue(100));

            {
                Paragraph w20 = new Paragraph().add("Signature of Faculty\n"+"Date:").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8).setBorder(Border.NO_BORDER).setBorderTop(new DashedBorder(.5f));
                Cell cell20 = new Cell().add(w20).setBorder(Border.NO_BORDER);
                tableSign.addCell(cell20);

                Paragraph w21 = new Paragraph().add(" ").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8).setBorder(Border.NO_BORDER);
                Cell cell21 = new Cell().add(w21).setBorder(Border.NO_BORDER);
                tableSign.addCell(cell21);

                Paragraph w22 = new Paragraph().add("Signature of Chair/Director\n"+"Date:").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8).setBorder(Border.NO_BORDER).setBorderTop(new DashedBorder(.5f));
                Cell cell22 = new Cell().add(w22).setBorder(Border.NO_BORDER);
                tableSign.addCell(cell22);

                Paragraph w201 = new Paragraph().add(" ").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8).setBorder(Border.NO_BORDER);
                Cell cell201 = new Cell().add(w201).setBorder(Border.NO_BORDER);
                tableSign.addCell(cell201);

                Paragraph w202 = new Paragraph().add("Signature of Chair/Director\n"+"Date:").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8).setBorder(Border.NO_BORDER).setBorderTop(new DashedBorder(.5f));
                Cell cell202 = new Cell().add(w202).setBorder(Border.NO_BORDER);
                tableSign.addCell(cell202);
            }
            document.add(tableSign);
        }

        document.close();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_PDF);
        headers.setContentDispositionFormData("attachment", "gradeChange.pdf");

        return ResponseEntity.ok()
                .headers(headers)
                .body(baos.toByteArray());
    }

    private static Cell createCell(String content) {
        return createCell(content, null, null);
    }


    private static Cell createCell(String content, Integer colSpan, Integer rowSpan) {

        if (colSpan != null && rowSpan != null) {
            Cell cell = new Cell(rowSpan, colSpan).setTextAlignment(TextAlignment.CENTER);
            cell.add(new Paragraph(content));
            return cell;
        }else if (colSpan != null && rowSpan == null) {
            Cell cell = new Cell(0, colSpan).setTextAlignment(TextAlignment.CENTER);
            cell.add(new Paragraph(content));
            return cell;
        }else if (colSpan == null && rowSpan != null) {
            Cell cell = new Cell(rowSpan, 0).setTextAlignment(TextAlignment.CENTER);
            cell.add(new Paragraph(content));
            return cell;
        }

        Cell cell = new Cell().setTextAlignment(TextAlignment.CENTER);
        cell.add(new Paragraph(content));
        return cell;
    }

    protected class PageEventHandler implements IEventHandler {
        private Document document;

        public void setDocument(Document document) {
            this.document = document;
        }

        public void handleEvent(Event event) {
            if (document == null) {
                throw new IllegalStateException("Document reference not set. Call setDocument() before using the event handler.");
            }

            PdfDocumentEvent docEvent = (PdfDocumentEvent) event;
            PdfDocument pdfDoc = docEvent.getDocument();
            PdfPage page = docEvent.getPage();
            int pageNumber = pdfDoc.getPageNumber(page);
            int totalPages = pdfDoc.getNumberOfPages();
            Rectangle pageSize = page.getPageSize();
            PdfCanvas pdfCanvas = new PdfCanvas(
                    page.newContentStreamBefore(), page.getResources(), pdfDoc);

            PdfFont helvetica = null;
            try {
                helvetica = PdfFontFactory.createFont(StandardFonts.HELVETICA);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            Table endingLine = new Table(6)
                    .setMarginTop(10)
                    .setWidth(UnitValue.createPercentValue(100))
                    .setHorizontalAlignment(HorizontalAlignment.CENTER);

            endingLine.addCell(createCell("This file was generated from UMS without any alteration or erasure.", 4, 1)
                    .setBorderRight(Border.NO_BORDER)
                    .setBorderLeft(Border.NO_BORDER)
                    .setBorderBottom(Border.NO_BORDER)
                    .setBorderTop(new DashedBorder(.7f))
                    .setFont(helvetica)
                    .setFontSize(8)
                    .setTextAlignment(TextAlignment.LEFT));

//            endingLine.addCell(createCell("Generated Time: " + currentDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"))+" \t"+"Page " + pageNumber + " of " + totalPages, 2,1)
            endingLine.addCell(createCell("Generated Time: " + currentDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")), 2,1)
                    .setBorderRight(Border.NO_BORDER)
                    .setBorderLeft(Border.NO_BORDER)
                    .setBorderBottom(Border.NO_BORDER)
                    .setBorderTop(new DashedBorder(.7f))
                    .setFont(helvetica)
                    .setFontSize(8)
                    .setTextAlignment(TextAlignment.RIGHT));

            // Set position of the table in the footer
            endingLine.setFixedPosition(pageSize.getLeft(), pageSize.getBottom(), pageSize.getWidth());

            // Add the footerTable to the document
            document.add(endingLine);

            pdfCanvas.release();
        }
    }
}