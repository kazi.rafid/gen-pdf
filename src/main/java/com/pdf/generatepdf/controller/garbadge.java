package com.pdf.generatepdf.controller;//package com.pdf.generatepdf.controller;
//
//public class garbadge {
//    private URL createCancelStudentshipOnlineApplicationPdf(OnlineApplication onlineApplication) throws ExecutionFailureException, IOException {
//        Student student = studentService.getProfile(onlineApplication.getStudent().getId(), null);
//
//        String advancedPayment = "";
//        StudentLedger studentLedger = transactionService.getStudentLedger(student.getId(), null, false);
//        if (studentLedger != null) {
//            if (studentLedger.getClosingBalance() > 0) {
//                advancedPayment = "Advanced Payment: " + Math.abs(studentLedger.getClosingBalance()) + " Taka.";
//            }
//        }
//
//        File directory = new File("/tmp");
//        if (!directory.exists()) directory.mkdir();
//
//        String formLocation = "/tmp/" + onlineApplication.getApplicationID() + "_online_application.pdf";
//
//        File file = new File(formLocation);
//        file.getParentFile().mkdirs();
//
//        PdfWriter writer = new PdfWriter(formLocation);
//
//        PdfDocument pdf = new PdfDocument(writer);
//        Document document = new Document(pdf, PageSize.A4);
//        document.setMargins(10, 20, 10, 20);        //Top Right Bottom Left
//
//
//        PdfFont helveticaBold = PdfFontFactory.createFont(StandardFonts.HELVETICA_BOLD);
//        PdfFont helvetica = PdfFontFactory.createFont(StandardFonts.HELVETICA);
//
//        PdfFont agencyFbFont = Utility.getAgencyFbPdfFont();
//
//
//        Paragraph p1 = new Paragraph()
//                .add(Utility.UNIVERSITY_NAME.toUpperCase())
//                .setTextAlignment(TextAlignment.CENTER)
//                .setFont(agencyFbFont)
//                .setFontSize(20)
//                .setBold()
//                .setMargin(0)
//                .setPadding(0);
//        document.add(p1);
//
//        Paragraph p2 = new Paragraph()
//                .add(Utility.UNIVERSITY_ADDRESS + "\n" + Utility.UNIVERSITY_PHONE + "; " + Utility.UNIVERSITY_EMAIL)
//                .setFontSize(10)
//                .setFont(helvetica)
//                .setTextAlignment(TextAlignment.CENTER)
//                .setMargin(0)
//                .setPadding(0);
//        document.add(p2);
//
//        Paragraph p3 = new Paragraph()
//                .add("Application for Admission Cancellation")
//                .setTextAlignment(TextAlignment.CENTER)
//                .setFont(helveticaBold)
//                .setFontSize(15)
//                .setMarginTop(10)
//                .setMarginBottom(-7)
//                .setPadding(0);
//        document.add(p3);
//
//
//        Paragraph p33 = new Paragraph()
//                .add("Application ID # " + onlineApplication.getApplicationID())
//                .setTextAlignment(TextAlignment.CENTER)
//                .setFont(helveticaBold)
//                .setFontSize(12)
//                .setMarginTop(10)
//                .setMarginBottom(-7)
//                .setPadding(0);
//        document.add(p33);
//
//
//        int fontSize = 10;
//        float fontSize8 = 10;
//
//        {
//            float[] signColumnWidth = {15, 32, 2, 15, 32};
//            Table tableSign = new Table(UnitValue.createPercentArray(signColumnWidth));
//            tableSign.setBorder(Border.NO_BORDER);
//            tableSign.setMarginTop(10);
//            tableSign.setWidth(UnitValue.createPercentValue(100));
//
//            Paragraph w1 = new Paragraph().add("Student Information:")
//                    .setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
//            Cell cell1 = new Cell(1, 5).add(w1).setBorder(Border.NO_BORDER);
//            tableSign.addCell(cell1);
//            {
//                Paragraph w11 = new Paragraph().add("Student Name").setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
//                Cell cell11 = new Cell().add(w11).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell11);
//
//                Paragraph w12 = new Paragraph().add(onlineApplication.getStudent().getName().getFullName()).setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
//                Cell cell12 = new Cell().add(w12).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell12);
//
//                Paragraph w13 = new Paragraph().add(" ").setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
//                Cell cell13 = new Cell().add(w13).setBorder(Border.NO_BORDER);
//                tableSign.addCell(cell13);
//
//
//                Paragraph w14 = new Paragraph().add("Student ID").setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
//                Cell cell14 = new Cell().add(w14).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell14);
//
//                Paragraph w15 = new Paragraph().add(student.getUgcId()).setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
//                Cell cell15 = new Cell().add(w15).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell15);
//            }
//
//            {
//                String fatherName = "";
//                if (student.getFather() != null && student.getFather().getName() != null) {
//                    fatherName = student.getFather().getName().getFullName().trim();
//                }
//                Paragraph w11 = new Paragraph().add("Father Name").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize);
//                Cell cell11 = new Cell().add(w11).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell11);
//
//                Paragraph w12 = new Paragraph().add(fatherName).setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize);
//                Cell cell12 = new Cell().add(w12).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell12);
//
//                Paragraph w13 = new Paragraph().add(" ").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize);
//                Cell cell13 = new Cell().add(w13).setBorder(Border.NO_BORDER);
//                tableSign.addCell(cell13);
//
//                Paragraph w14 = new Paragraph().add("Student Code").setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
//                Cell cell14 = new Cell().add(w14).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell14);
//
//                Paragraph w15 = new Paragraph().add(student.getId()).setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
//                Cell cell15 = new Cell().add(w15).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell15);
//            }
//
//            {
//                String emailAddress = "";
//                if (student.getEmailAddressList() != null) {
//                    emailAddress = student.getEmailAddressList().get(0).getAddress();
//                }
//                if (emailAddress.isEmpty()) {
//                    emailAddress = student.getId() + "@seu.edu.bd";
//                }
//                Paragraph w11 = new Paragraph().add("Email").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize);
//                Cell cell11 = new Cell().add(w11).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell11);
//
//                Paragraph w12 = new Paragraph().add(emailAddress).setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize);
//                Cell cell12 = new Cell().add(w12).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell12);
//
//                Paragraph w13 = new Paragraph().add(" ").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize);
//                Cell cell13 = new Cell().add(w13).setBorder(Border.NO_BORDER);
//                tableSign.addCell(cell13);
//
//                Paragraph w14 = new Paragraph().add("Program Name").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize);
//                Cell cell14 = new Cell().add(w14).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell14);
//
//                Paragraph w15 = new Paragraph().add(student.getAcademicProgram().getName() + ", Batch: " + student.getBatchId()).setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize);
//                Cell cell15 = new Cell().add(w15).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell15);
//            }
//
//            {
//                Paragraph w11 = new Paragraph().add("Mobile No.").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize);
//                Cell cell11 = new Cell().add(w11).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell11);
//
//                Paragraph w12 = new Paragraph().add(onlineApplication.getPhone()).setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize);
//                Cell cell12 = new Cell().add(w12).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell12);
//
//                Paragraph w13 = new Paragraph().add(" ").setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
//                Cell cell13 = new Cell().add(w13).setBorder(Border.NO_BORDER);
//                tableSign.addCell(cell13);
//
//                Paragraph w14 = new Paragraph().add("Admitted Semester").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize - 2);
//                Cell cell14 = new Cell().add(w14).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell14);
//
//                Paragraph w15 = new Paragraph().add(student.getEnrollingSemester().getLabel()).setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize);
//                Cell cell15 = new Cell().add(w15).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell15);
//            }
//
//            document.add(tableSign);
//        }
//
//        {
//            String priority = "";
//            if (onlineApplication.getPriority().equals(OnlineApplication.OnlineApplicationPriority.NORMAL)) {
//                priority = "Normal";
//            } else if (onlineApplication.getPriority().equals(OnlineApplication.OnlineApplicationPriority.URGENT)) {
//                priority = "Urgent";
//            } else if (onlineApplication.getPriority().equals(OnlineApplication.OnlineApplicationPriority.IMMEDIATE)) {
//                priority = "Immediate";
//            }
//
//            float[] signColumnWidth = {20, 40, 2, 20, 15};
//            Table tableSign = new Table(UnitValue.createPercentArray(signColumnWidth));
//            tableSign.setBorder(Border.NO_BORDER);
//            tableSign.setMarginTop(10);
//            tableSign.setWidth(UnitValue.createPercentValue(100));
//
//            Paragraph w1 = new Paragraph().add("Application Information:")
//                    .setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
//            Cell cell1 = new Cell(1, 8).add(w1).setBorder(Border.NO_BORDER);
//            tableSign.addCell(cell1);
//            {
//                Paragraph w19 = new Paragraph().add("Applied Date:").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8);
//                Cell cell19 = new Cell().add(w19).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell19);
//
//                Paragraph w20 = new Paragraph().add(onlineApplication.getAppliedTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))).setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8);
//                Cell cell20 = new Cell().add(w20).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell20);
//
//                Paragraph w200 = new Paragraph().add(" ").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8);
//                Cell w2001 = new Cell().add(w200).setBorder(Border.NO_BORDER);
//                tableSign.addCell(w2001);
//
//
//                Paragraph w190 = new Paragraph().add("Application Priority:").setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8);
//                Cell cell190 = new Cell().add(w190).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell190);
//
//                Paragraph w2000 = new Paragraph().add(priority).setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8);
//                Cell cell2000 = new Cell().add(w2000).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell2000);
//            }
//
//            document.add(tableSign);
//        }
//
//        {
//            float[] signColumnWidth = {100};
//            Table tableSign = new Table(UnitValue.createPercentArray(signColumnWidth));
//            tableSign.setBorder(Border.NO_BORDER);
//            tableSign.setMarginTop(10);
//            tableSign.setWidth(UnitValue.createPercentValue(100));
//
//            Paragraph w1 = new Paragraph().add("Reason for Admission Cancellation:")
//                    .setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
//            Cell cell1 = new Cell().add(w1).setBorder(Border.NO_BORDER);
//            tableSign.addCell(cell1);
//            {
//                Paragraph w20 = new Paragraph().add(onlineApplication.getCancelAdmission().getReason()).setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8);
//                Cell cell20 = new Cell().add(w20).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell20);
//            }
//            document.add(tableSign);
//        }
//
//        {
//            float[] signColumnWidth = {40, 60};
//            Table tableSign = new Table(UnitValue.createPercentArray(signColumnWidth));
//            tableSign.setBorder(Border.NO_BORDER);
//            tableSign.setMarginTop(10);
//            tableSign.setWidth(UnitValue.createPercentValue(100));
//
//            Paragraph w1 = new Paragraph().add("Clearance Information:")
//                    .setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
//            Cell cell1 = new Cell(1, 8).add(w1).setBorder(Border.NO_BORDER);
//            tableSign.addCell(cell1);
//
//            {
//                OnlineApplicationApprover departmentApprover = onlineApplication.getOnlineApplicationApproverList().get(0);
//                OnlineApplication.OnlineApplicationDecision decision = departmentApprover.getDecision();
//
//                String decisionString = "";
//                if (decision.equals(OnlineApplication.OnlineApplicationDecision.REVIEWING)) {
//                    decisionString = "Decision Pending";
//                } else if (decision.equals(OnlineApplication.OnlineApplicationDecision.PROCESSING)) {
//                    decisionString = "-";
//                } else if (decision.equals(OnlineApplication.OnlineApplicationDecision.APPROVED)) {
//                    decisionString = "The student surrendered the student card.";
//                } else if (decision.equals(OnlineApplication.OnlineApplicationDecision.REJECTED)) {
//                    decisionString = "The department has not provided any clearance.";
//                }
//
//                Paragraph w19 = new Paragraph().add(departmentApprover.getOffice()).setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8);
//                Cell cell19 = new Cell().add(w19).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell19);
//
//                Paragraph w20 = new Paragraph().add(decisionString).setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8);
//                Cell cell20 = new Cell().add(w20).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell20);
//            }
//
//            {
//                OnlineApplicationApprover departmentApprover = onlineApplication.getOnlineApplicationApproverList().get(1);
//                OnlineApplication.OnlineApplicationDecision decision = departmentApprover.getDecision();
//
//                String decisionString = "";
//                if (decision.equals(OnlineApplication.OnlineApplicationDecision.REVIEWING)) {
//                    decisionString = "Decision Pending";
//                } else if (decision.equals(OnlineApplication.OnlineApplicationDecision.PROCESSING)) {
//                    decisionString = "-";
//                } else if (decision.equals(OnlineApplication.OnlineApplicationDecision.APPROVED)) {
//                    decisionString = "The student has no advised/registered course or does not have any pending grades for any semester.";
//                } else if (decision.equals(OnlineApplication.OnlineApplicationDecision.REJECTED)) {
//                    decisionString = "The department has not provided any clearance.";
//                }
//
//                Paragraph w19 = new Paragraph().add(departmentApprover.getOffice()).setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8);
//                Cell cell19 = new Cell().add(w19).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell19);
//
//                Paragraph w20 = new Paragraph().add(decisionString).setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8);
//                Cell cell20 = new Cell().add(w20).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell20);
//            }
//
//            {
//                OnlineApplicationApprover departmentApprover = onlineApplication.getOnlineApplicationApproverList().get(2);
//                OnlineApplication.OnlineApplicationDecision decision = departmentApprover.getDecision();
//
//                String decisionString = "";
//                if (decision.equals(OnlineApplication.OnlineApplicationDecision.REVIEWING)) {
//                    decisionString = "Decision Pending";
//                } else if (decision.equals(OnlineApplication.OnlineApplicationDecision.PROCESSING)) {
//                    decisionString = "-";
//                } else if (decision.equals(OnlineApplication.OnlineApplicationDecision.APPROVED)) {
//                    decisionString = "The student has no dues related to library office.";
//                } else if (decision.equals(OnlineApplication.OnlineApplicationDecision.REJECTED)) {
//                    decisionString = "The department has not provided any clearance.";
//                }
//
//                Paragraph w19 = new Paragraph().add(departmentApprover.getOffice()).setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8);
//                Cell cell19 = new Cell().add(w19).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell19);
//
//                Paragraph w20 = new Paragraph().add(decisionString).setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8);
//                Cell cell20 = new Cell().add(w20).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell20);
//            }
//
//            {
//                OnlineApplicationApprover departmentApprover = onlineApplication.getOnlineApplicationApproverList().get(3);
//                OnlineApplication.OnlineApplicationDecision decision = departmentApprover.getDecision();
//
//                String decisionString = "";
//                if (decision.equals(OnlineApplication.OnlineApplicationDecision.REVIEWING)) {
//                    decisionString = "Decision Pending";
//                } else if (decision.equals(OnlineApplication.OnlineApplicationDecision.PROCESSING)) {
//                    decisionString = "-";
//                } else if (decision.equals(OnlineApplication.OnlineApplicationDecision.APPROVED)) {
//                    decisionString = "The student has no dues." + (advancedPayment.isEmpty() ? "" : " " + advancedPayment);
//                } else if (decision.equals(OnlineApplication.OnlineApplicationDecision.REJECTED)) {
//                    decisionString = "The department has not provided any clearance.";
//                }
//
//                Paragraph w19 = new Paragraph().add(departmentApprover.getOffice()).setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8);
//                Cell cell19 = new Cell().add(w19).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell19);
//
//                Paragraph w20 = new Paragraph().add(decisionString).setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8);
//                Cell cell20 = new Cell().add(w20).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell20);
//            }
//
//            {
//                OnlineApplicationApprover departmentApprover = onlineApplication.getOnlineApplicationApproverList().get(4);
//                OnlineApplication.OnlineApplicationDecision decision = departmentApprover.getDecision();
//
//                String decisionString = "";
//                if (decision.equals(OnlineApplication.OnlineApplicationDecision.REVIEWING)) {
//                    decisionString = "Decision Pending";
//                } else if (decision.equals(OnlineApplication.OnlineApplicationDecision.PROCESSING)) {
//                    decisionString = "-";
//                } else if (decision.equals(OnlineApplication.OnlineApplicationDecision.APPROVED)) {
//                    decisionString = "The student has no academic or disciplinary defaulter.";
//                } else if (decision.equals(OnlineApplication.OnlineApplicationDecision.REJECTED)) {
//                    decisionString = "The department has not provided any clearance.";
//                }
//
//                Paragraph w19 = new Paragraph().add(departmentApprover.getOffice()).setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8);
//                Cell cell19 = new Cell().add(w19).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell19);
//
//                Paragraph w20 = new Paragraph().add(decisionString).setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8);
//                Cell cell20 = new Cell().add(w20).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell20);
//            }
//
//            document.add(tableSign);
//        }
//
//        {
//            float[] signColumnWidth = {40, 60};
//            Table tableSign = new Table(UnitValue.createPercentArray(signColumnWidth));
//            tableSign.setBorder(Border.NO_BORDER);
//            tableSign.setMarginTop(10);
//            tableSign.setWidth(UnitValue.createPercentValue(100));
//
//            Paragraph w1 = new Paragraph().add("Approval Information:")
//                    .setTextAlignment(TextAlignment.LEFT).setFont(helveticaBold).setFontSize(fontSize);
//            Cell cell1 = new Cell(1, 8).add(w1).setBorder(Border.NO_BORDER);
//            tableSign.addCell(cell1);
//
//            for (OnlineApplicationApprover onlineApplicationApprover : onlineApplication.getOnlineApplicationApproverList()) {
//                String office = onlineApplicationApprover.getOffice();
//                OnlineApplication.OnlineApplicationDecision decision = onlineApplicationApprover.getDecision();
//                String decisionTakenBy = onlineApplicationApprover.getDecisionTakenBy();
//                LocalDateTime decisionTakenTime = onlineApplicationApprover.getDecisionTakenTime();
//
//                Paragraph w19 = new Paragraph().add(onlineApplicationApprover.getOffice()).setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8);
//                Cell cell19 = new Cell().add(w19).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell19);
//
//                Paragraph w20 = new Paragraph().add(statusString(decision, decisionTakenBy, decisionTakenTime)).setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize8);
//                Cell cell20 = new Cell().add(w20).setBorder(new SolidBorder(1));
//                tableSign.addCell(cell20);
//            }
//            document.add(tableSign);
//        }
//
//        {
//            float[] signColumnWidth = {50, 50};
//            Table tableSign = new Table(UnitValue.createPercentArray(signColumnWidth));
//            tableSign.setBorder(Border.NO_BORDER);
//            tableSign.setMarginTop(10);
//            tableSign.setWidth(UnitValue.createPercentValue(100));
//
//            Paragraph w19 = new Paragraph().add("Signature of Admission Officer").setTextAlignment(TextAlignment.CENTER).setFont(helvetica).setFontSize(fontSize8);
//            Cell cell19 = new Cell().add(w19).setBorder(new SolidBorder(1));
//            tableSign.addCell(cell19);
//
//            Paragraph w20 = new Paragraph().add("Signature of former student").setTextAlignment(TextAlignment.CENTER).setFont(helvetica).setFontSize(fontSize8);
//            Cell cell20 = new Cell().add(w20).setBorder(new SolidBorder(1));
//            tableSign.addCell(cell20);
//
//            Paragraph w190 = new Paragraph().add(".").setFontColor(ColorConstants.WHITE).setTextAlignment(TextAlignment.CENTER).setFont(helvetica).setFontSize(fontSize8);
//            Cell cell190 = new Cell().add(w190).setMarginTop(12f).setMarginBottom(12f).setBorder(new SolidBorder(1));
//            tableSign.addCell(cell190);
//
//            Paragraph w1900 = new Paragraph().add(".").setFontColor(ColorConstants.WHITE).setTextAlignment(TextAlignment.CENTER).setFont(helvetica).setFontSize(fontSize8);
//            Cell cell1900 = new Cell().add(w1900).setBorder(new SolidBorder(1));
//            tableSign.addCell(cell1900);
//
//            Paragraph w25 = new Paragraph().add("This is computer generated document. " + "Generated on " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))).setTextAlignment(TextAlignment.LEFT).setFont(helvetica).setFontSize(fontSize - 3);
//            Cell cell25 = new Cell(1, 2).add(w25).setBorder(Border.NO_BORDER);
//            tableSign.addCell(cell25);
//
//            document.add(tableSign);
//        }
//
//        document.close();
//
//        URL url = fileServerService.uploadFileToDownload(file, "pdf");
//        return url;
//    }
//
//}

//
//import com.itextpdf.layout.borders.Border;
//import com.itextpdf.layout.borders.DashedBorder;
//import com.itextpdf.layout.element.Cell;
//import com.itextpdf.layout.element.Paragraph;
//import com.itextpdf.layout.element.Table;
//import com.itextpdf.layout.properties.HorizontalAlignment;
//import com.itextpdf.layout.properties.TextAlignment;
//import com.itextpdf.layout.properties.UnitValue;
//import com.itextpdf.layout.properties.VerticalAlignment;
//
//import java.time.LocalDateTime;
//import java.time.format.DateTimeFormatter;
//
//Cell universityCell = createCell("");
//        universityCell.setBorder(Border.NO_BORDER);
//
//Table tableStudentInformation = new Table(14)
//        .setMarginTop(10)
//        .setWidth(UnitValue.createPercentValue(70))
//        .setHorizontalAlignment(HorizontalAlignment.CENTER);
//
//        tableStudentInformation.addCell(createCell("Student Code").setFont(generalFontBold)
//                .setFontSize(10)
//                .setTextAlignment(TextAlignment.CENTER)
//                .setPaddings(0,-15,0,-15));
//
//String studentCode = "2020000000055";
//        for (char digitChar : studentCode.toCharArray()) {
//        tableStudentInformation.addCell(createCell(String.valueOf(digitChar)).setFont(generalFontBold)
//                    .setFontSize(10)
//                    .setTextAlignment(TextAlignment.CENTER)
//                    .setPaddings(0, 0,0,0));
//        }
//
//        tableStudentInformation.addCell(createCell("Name").setFont(generalFontRegular)
//                .setFontSize(10).setPadding(-2)
//                .setTextAlignment(TextAlignment.CENTER)
//                .setVerticalAlignment(VerticalAlignment.MIDDLE)
//                .setPaddings(0, 0,0,0));
//
//        tableStudentInformation.addCell(new Cell(0, 13).add(new Paragraph("Md. Shakhawat Hossain"))
//        .setFont(generalFontRegular)
//                .setFontSize(10)
//                .setPaddingLeft(4));
//
//        tableStudentInformation.addCell(createCell("Program").setFont(generalFontRegular)
//                .setFontSize(10)
//                .setTextAlignment(TextAlignment.CENTER)
//                .setVerticalAlignment(VerticalAlignment.MIDDLE)
//                .setPaddings(0, 0,0,0));
//
//        tableStudentInformation.addCell(new Cell(0, 13).add(new Paragraph("BSc in CSE, Batch # 54"))
//        .setFont(generalFontRegular)
//                .setFontSize(10)
//                .setPaddingLeft(4));
//
//        document.add(tableStudentInformation);
//
//        document.add(new Paragraph("Registered Courses:")
//                .setFont(generalFontBold)
//                .setFontSize(10)
//                .setMarginTop(12)
//                .setTextAlignment(TextAlignment.LEFT));
//
//
//// Adding course list table
//Table tableCourseInformation = new Table(6)
//        .setMarginTop(-5)
//        .setWidth(UnitValue.createPercentValue(100))
//        .setHorizontalAlignment(HorizontalAlignment.CENTER);
//
//        tableCourseInformation.addCell(createCell("Course Code").setFont(generalFontBold)
//                .setFontSize(10)
//                .setTextAlignment(TextAlignment.LEFT)
//                .setVerticalAlignment(VerticalAlignment.MIDDLE)
//                .setPaddingLeft(4));
//
//        tableCourseInformation.addCell(createCell("Course Title                         ", 3, 1).setFont(generalFontBold)
//                .setFontSize(10)
//                .setTextAlignment(TextAlignment.LEFT)
//                .setVerticalAlignment(VerticalAlignment.MIDDLE)
//                .setPaddingLeft(4));
//
//
//        tableCourseInformation.addCell(createCell("Registration\n" +
//        "Type").setFont(generalFontBold)
//                .setFontSize(10)
//                .setTextAlignment(TextAlignment.CENTER)
//                .setVerticalAlignment(VerticalAlignment.MIDDLE));
//
//        tableCourseInformation.addCell(createCell("Invigilator's\n" +
//        "Signature").setFont(generalFontBold)
//                .setFontSize(10)
//                .setTextAlignment(TextAlignment.CENTER)
//                .setVerticalAlignment(VerticalAlignment.MIDDLE));
//
//
//        ///Course List
//        tableCourseInformation.addCell(createCell("CSE489.1").setFont(generalFontRegular)
//                .setFontSize(10)
//                .setTextAlignment(TextAlignment.LEFT)
//                .setPaddingLeft(4));
//
//        tableCourseInformation.addCell(createCell("Internship", 3, 1).setFont(generalFontRegular)
//                .setFontSize(10)
//                .setTextAlignment(TextAlignment.LEFT)
//                .setPaddingLeft(4));
//
//
//        tableCourseInformation.addCell(createCell("Regular"+"\n"+" \n").setFont(generalFontRegular)
//                .setFontSize(10)
//                .setTextAlignment(TextAlignment.CENTER));
//
//        tableCourseInformation.addCell(createCell(""));
//
//
//        tableCourseInformation.addCell(createCell("CSE459.1").setFont(generalFontRegular)
//                .setFontSize(10)
//                .setTextAlignment(TextAlignment.LEFT)
//                .setPaddingLeft(4));
//
//        tableCourseInformation.addCell(createCell("Research ", 3, 1).setFont(generalFontRegular)
//                .setFontSize(10)
//                .setTextAlignment(TextAlignment.LEFT)
//                .setPaddingLeft(4));
//
//
//        tableCourseInformation.addCell(createCell("Regular"+"\n"+" \n").setFont(generalFontRegular)
//                .setFontSize(10)
//                .setTextAlignment(TextAlignment.CENTER));
//
//        tableCourseInformation.addCell(createCell(""));
//
//
//LocalDateTime currentDateTime = LocalDateTime.now();
//        tableCourseInformation.addCell(createCell("Generated Time: " + currentDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"))+"\n"+" \n", 2,1)
//        .setBorderRight(Border.NO_BORDER)
//                .setBorderLeft(Border.NO_BORDER)
//                .setBorderBottom(new DashedBorder(.7f))
//        .setFont(generalFontRegular)
//                .setFontSize(8)
//                .setTextAlignment(TextAlignment.LEFT));
//
//        tableCourseInformation.addCell(createCell("This file was generated from UMS without any alteration or erasure."+"\n"+" \n", 4, 1).setFont(generalFontRegular)
//                .setBorderRight(Border.NO_BORDER)
//                .setBorderLeft(Border.NO_BORDER)
//                .setBorderBottom(new DashedBorder(.7f))
//        .setFontSize(8)
//                .setTextAlignment(TextAlignment.RIGHT));
//
//        document.add(tableCourseInformation);
//
//
//        document.add(new Paragraph("Instructions to the Examine:")
//                .setFont(generalFontBold)
//                .setFontSize(10)
//                .setMarginTop(20)
//                .setTextAlignment(TextAlignment.LEFT));
//
//
//
//
//String[] dataArray = {
//        "Print this Admit Card on an A4 size paper. Colour print is not mandatory.",
//        "Admit card is valid only if the examines photograph is legibly printed.",
//        "The examine must bring the ‘Admit Card’ and ‘Student ID’ to the examination hall and show those to the invigilators when they ask.",
//        "The examine must arrive in the examination hall at least 15 minutes before the start of the examination.",
//        "The examine must comply with the directions given by the invigilators at the examination hall.",
//        "Use of cell phones, programmable calculators, smartwatches, and other electronic devices is prohibited in the examination hall.",
//        "The examine involved in unfair means/misconduct in the examination hall will be expelled from the examination."
//};
//
//// Adding course list table
////        Table tableExamineInstruction = new Table(1)
////                .setMarginTop(-5)
////                .setWidth(UnitValue.createPercentValue(100))
////                .setHorizontalAlignment(HorizontalAlignment.CENTER);
//
//
//        document.add(createCellWithArray(dataArray)
//                .setPaddingLeft(20)
//                .setMarginTop(-5));
//
////        document.add(createUnorderedList(dataArray));
//
////        document.add(tableExamineInstruction);