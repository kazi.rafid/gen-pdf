package com.pdf.generatepdf.type;

public class OfficeAccess {
    private String office;
    private String username;
    private String dateTime;

    public OfficeAccess(String office, String username, String dateTime) {
        this.office = office;
        this.username = username;
        this.dateTime = dateTime;
    }

    // Getter methods
    public String getOffice() {
        return office;
    }

    public String getUsername() {
        return username;
    }

    public String getDateTime() {
        return dateTime;
    }
}
